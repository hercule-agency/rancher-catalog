## What is inside WordPress Stack?
* MariaDB Database
* WordPress (php7/apache)

## Install your theme and stuffs

Install tools : 
```
apt-get update \
    && apt-get -y install wget vim-tiny unzip zip
```

Get your files : 
```
wget http://www.hercule.co/mystuffs.zip
unzip mystuffs.zip
```

## Good to know

- The real files are in /opt/bitnami/wordpress/ , the persistent files are in /bitnami (mainly /bitnami/wordpress)
- To persist website and database data, two volumes are created: mariadb_data, wordpress_data.  
- From the host, you can have the path to the volume with command : 
  `docker volume inspect --format '{{ .Mountpoint }}' <container-name>`

- Debug : check logs with `tail /opt/bitnami/apache/logs/access_log`  or `tail /opt/bitnami/apache/logs/error_log`

## Issues

- Be sure that you have a "/wp-content/healthcheck.php" that return code "200"
- Be sure to add a symlink if needed : `ln -s /bitnami/wordpress/.htaccess /opt/bitnami/wordpress/.htaccess`


## Compatibility Notes

* Version v0.2-bitnami has some known [issue](https://github.com/bitnami/bitnami-docker-testlink/issues/17#issuecomment-261783035) with Docker overlay and overlay2 storage driver. Please try to switch to aufs or devicemapper.


