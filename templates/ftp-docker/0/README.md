# Hercule vsftpd

Docker image of vsftpd server based on Alpine 3.4 

## Comment ça marche ?

Ce service doit être lancé au travers d'une "Stack" sur **chaque host Rancher** (car le port 21 s'ouvre au niveau du host) qu iveut accéder en FTP à un répertoire de sa stack.

Avant de commencer, il faut connaitre le path vers les données physiques du container. 
Pour cela, deux solutions : 

    - [RECOMMANDÉ], Quand la Stack du Wordpress a été créé, cela a été fait avec un volume persistant dans root (ex: `/srv/mon-projet/file`)

    - Si le volume est directement dans les entrailles de Docker, il est possible d'obtenir le path en lançant cette commande sur le serveur host :
    `docker volume inspect --format '{{ .Mountpoint }}' <nom-complet du volume>`


ℹ️ Pour donner accès en FTP aux fichiers d'une stack existante sur l'host concerné, il faut créer un volume entre les fichiers du host et la "home" du user FTP. 
Ex : `/srv/mon-projet/files:/home/smith` 

## How to ?

    - Dans Rancher, allez dans l'interface pour "upgrader" le service, dans l'onglet "Volumes", rajouter le volume nécéssaire dans le dossier accessible par le nouvel utilisateur FTP. 
    Syntaxe : *<path sur le host>*`:`*<dossier du user FTP>*
    Ex : `/srv/mon-projet/files:/home/smith` 

    - Upgrader le service pour confirmer les changements

    - Une fois le service Hercule-VsFtpd actif, il faut se connecter au shell du container et executer la commande `add-ftp-user` pour créer un nouvel utilisateur FTP. 
    (Ce script custom est dans /usr/sbin/add-ftp-user.sh)
    Ex : `add-ftp-user smith G00dP4ssword ` 
    
    - C'est tout ! Vous pouvez vous connecter en FTP !

## Client FTP

Host : l'ip du host
User : celui saisi via la commande add-ftp-user
Mdp  : celui saisi via la commande add-ftp-user
Port : 21
Type d'authentification : Normale
